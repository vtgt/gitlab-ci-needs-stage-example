FROM alpine:latest AS builder

RUN echo "some commands to install required build tools" > .build-tools-installed
RUN echo "sleep 30"


FROM builder AS emulator

RUN echo "some additional commands to install the tools needed to run the app in emulation" > .emulator-installed
RUN echo "sleep 20"


FROM alpine:latest AS uploader

RUN echo "some other commands would install the tools needed to upload the final build package" > .uploading-tools-installed
RUN echo "sleep 10"
