# gitlab-ci-needs-stage-example

And example to help illustrate some of the issues related to stage vs DAG based CI pipelines.
Written to contribute to the conversation around support for a "needs:stage" feature, from:
https://gitlab.com/gitlab-org/gitlab/-/issues/220758

In particular, the pipeline currently committed demonstrates how the use of "needs" while still expecting stage-based behaviour can lead to very undesirable outcomes. In this case, the 'app' gets uploaded, despite tests failing.

It appears that the 'needs' / DAG based model assumes that it will completely replace stages, so that any job that needs another job will ignore the state of all other jobs, even in earlier stages, and even when "when: on_success" is explicitly stated.

The only solution that keeps the benefits of the DAG-based model is to explicitly state every requirement - even that earlier stages MUST pass. And most of these "needs:" will have to have "artifacts: false" on them.

The whole thing tosses out all the benefits of stages: namely the implicit expression of dependencies, and leaves us with something verbose and difficult-to-read.
